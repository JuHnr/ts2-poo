
/*CUSTOMER*/

//création d'un type alias pour l'adresse
type Address = { street: string, city: string, postalCode: string, country: string }

class Customer {
    // l'adresse est de type Address ou peut ne pas être définie (undefined)
    address: Address | undefined;

    constructor(
        public customerId: number,
        public name: string,
        public email: string
    ) { }

    /**
    *Défini l'addresse du client
    *@param address objet de type Address {street, city, postalCode, country}
    *@returns void
    *
    */
    setAddress(address: Address): void {
        this.address = address;
    }

    //afficher l'adresse 
    displayAddress(): string {
        if (this.address) {
            return `Address: ${this.address.street} ${this.address.postalCode} ${this.address.city}, ${this.address.country}`;
        } else {
            return 'Address : No address found';
        }
    }

    //afficher toutes les infos du client
    displayInfo(): string {
        return `CustomerID : ${this.customerId}, Name : ${this.name}, Email : ${this.email}, ${this.displayAddress()}`;
    }


}

/* PRODUCT */

//création dun type alias pour les dimensions du colis
type Dimensions = { length: number, width: number, height: number };

class Product {

    constructor(
        public productId: number,
        public name: string,
        public weight: number,
        public price: number,
        public dimensions: Dimensions
    ) { }

    displayDetails(): string {
        return `productID : ${this.productId}, Name : ${this.name}, Weight : ${this.weight} kg, Price : ${this.price}€, Dimensions : ${this.dimensions.length}x${this.dimensions.width}x${this.dimensions.height}`;
    }
}

/* CLOTHING */

//création d'un Enum pour définir les valeurs possibles pour ClothingSize
enum ClothingSize {
    size_xs = "XS",
    size_s = "S",
    size_m = "M",
    size_l = "L",
    size_xl = "XL",
    size_xxl = "XXL"
}

//la classe Clothing hérite de la classe Product
class Clothing extends Product {
    constructor(
        public productId: number,
        public name: string,
        public weight: number,
        public price: number,
        public dimensions: Dimensions,
        public size: ClothingSize
    ) {
        super(productId, name, weight, price, dimensions)
    }

    displayDetails(): string {
        const productDetails = super.displayDetails();
        return productDetails + ` Size : ${this.size}`;
    }
}

/* SHOE */
//création d'un Enum pour définir les valeurs possibles pour ShoeSize
enum ShoeSize {
    size_36 = "36",
    size_37 = "37",
    size_38 = "38",
    size_39 = "39",
    size_40 = "40",
    size_41 = "41",
    size_42 = "42",
    size_43 = "43",
    size_44 = "44",
    size_45 = "45",
    size_46 = "46",
}

//la classe Clothing hérite de la classe Product
class Shoe extends Product {
    constructor(
        public productId: number,
        public name: string,
        public weight: number,
        public price: number,
        public dimensions: Dimensions,
        public size: ShoeSize
    ) {
        super(productId, name, weight, price, dimensions)
    }

    displayDetails(): string {
        const productDetails = super.displayDetails();
        return productDetails + ` Size : ${this.size}`;
    }
}


/* ORDER */
class Order {
    // la livraison est de type Deliverable ou peut ne pas être définie (undefined)
    delivery: Deliverable | undefined;

    constructor(
        public orderId: number,
        public customer: Customer,
        public productList: Product[],
        public orderDate: string,
    ) { }


    addProduct(product: Product): void {
        this.productList.push(product);
    }


    /**
    *Itère dans les produit de productList, si l'id du produit est différent de l'id indiqué, alors le produit est conservé dans la liste (= supprime le produit à l'id indiqué)
    *@param productId id du produit que l'on souhaite supprimer
    *@returns void : modifie productList sans la retourner. 
    *
    */
    removeProduct(productId: number): void {
        this.productList = this.productList.filter(product => product.productId !== productId);
    }

    calculateWeight(): number {
        let totalWeight: number = 0;
        this.productList.forEach(product => {
            totalWeight += product.weight
        });
        return totalWeight;
    }

    calculateTotal(): number {
        let totalPrice: number = 0;
        this.productList.forEach(product => {
            totalPrice += product.price
        });
        return totalPrice;
    }


    /**
    *
    *@param delivery instance standard ou express (qui implémentent l'interface Delivery, avec les méthodes calculateShippingFee et estimateDeliveryTime)
    *@returns void 
    *
    */
    setDelivery(delivery: Deliverable): void {
        this.delivery = delivery;
    }


    displayProductList(): string[] {
        return this.productList.map(product => product.displayDetails() + "\n");
    }

    displayOrder(): string {
        return `orderID : ${this.orderId}
        \nOrder Date : ${this.orderDate}
        \nCustomer : \n${this.customer.displayInfo()} 
        \nProducts List : \n${this.displayProductList()} 
        \nTotal weight : ${this.calculateWeight()} kg 
        \nTotal price : ${this.calculateTotal()} €
        `;
    }


    /**
    *
    *calcule le coût de la livraison selon le type de livraison choisi (this.delivery) et le poids du colis (this.calculateWeight()),
    *@returns le coût de la livraison ou undefined si aucune livraison n'a été sélectionnée
    *
    */
    calculateShippingCosts(): number | undefined {
        if (this.delivery) {
            return this.delivery?.calculateShippingFee(this.calculateWeight());
        }
    }

    /**
    *
    *estime le délai de livraison estimé selon le type de livraison choisi (this.delivery) et le poids du colis (this.calculateWeight()), 
    *@returns le délai de livraison estimé ou undefined si aucune livraison n'a été sélectionnée
    *
    */
    estimateDeliveryTime(): number | undefined {
        if (this.delivery) {
            return this.delivery?.estimateDeliveryTime(this.calculateWeight());
        }
    }
}

/*INTERFACE*/

interface Deliverable {
    estimateDeliveryTime(weight: number): number;
    calculateShippingFee(weight: number): number;
}

//Création des classes implémentant l'interface Deliverable
class StandardDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        return weight < 10 ? 7 : 10
    };

    calculateShippingFee(weight: number): number {
        if (weight < 1) {
            return 5
        } else if (weight > 5) {
            return 20
        } else {
            return 10
        }
    };
}

class ExpressDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        return weight <= 5 ? 1 : 3;
    }

    calculateShippingFee(weight: number): number {
        if (weight < 1) {
            return 8
        } else if (weight > 5) {
            return 30
        } else {
            return 14
        }
    };
}



/* UTILISATION */

//créé un nouveau client et lui attribue son addresse
const vincent = new Customer(2, "Vincent", "vincent@gmail.com");
vincent.setAddress({ street: "1 rue de l'église", city: "Paris", postalCode: "75000", country: "France" });

//créé une nouvelle commande
const vincentOrder = new Order(1, vincent, [], "24/02/2024");

//créé de nouveaux produits
const lunettes = new Product(1, "lunettes", 0.2, 16, { length: 2, width: 2, height: 2 });
const rasoir = new Product(2, "rasoir", 0.1, 12, { length: 2, width: 2, height: 2 })
const tshirt = new Clothing(3, "tshirt", 0.7, 25, { length: 2, width: 2, height: 2 }, ClothingSize.size_m);
const sneakers = new Shoe(4, "sneakers", 0.8, 60, { length: 2, width: 2, height: 2 }, ShoeSize.size_40);

//créé les modes de livraisons
const standard = new StandardDelivery();
const express = new ExpressDelivery();

//ajoute des produits dans la liste
vincentOrder.addProduct(lunettes);
vincentOrder.addProduct(rasoir);
vincentOrder.addProduct(tshirt);
vincentOrder.addProduct(sneakers);
console.log(vincentOrder.displayOrder());

//supprime le produit d'id 1 (lunettes)
vincentOrder.removeProduct(1);
console.log("A product was removed from order :", vincentOrder.orderId);
console.log(vincentOrder.displayProductList());

//sélectionne la livraison standard pour la commande
vincentOrder.setDelivery(standard);
console.log("Standard Shipping Costs: " , vincentOrder.calculateShippingCosts())
console.log("Standard Delivery Time: ", vincentOrder.estimateDeliveryTime())

//sélectionne la livraison express pour la commande
vincentOrder.setDelivery(express);
console.log("Express Shipping Costs: " , vincentOrder.calculateShippingCosts())
console.log("Express Delivery Time: ", vincentOrder.estimateDeliveryTime())