# SimplonFactory App

## Contexte du projet

*Ce projet est réalisé dans le cadre d'un cours sur la Programmation Orientée Objet*

SimplonFactory est une entreprise innovante dans le secteur de la fabrication et de la vente en ligne.
La mission est de créer un système qui gère les commandes des clients, le catalogue de produits, et le processus de livraison. Ce système devra permettre de créer et de gérer des clients (Customer), de gérer un inventaire de produits (Product) avec des spécificités telles que les vêtements (Clothing) et les chaussures (Shoes), et de gérer des commandes (Order) avec une logique de livraison flexible (Deliverable).

**Notions abordées**

TypeScript et Programmation Orientée Objet :

- classes et objets, 
- types (type alias, Enums), 
- héritage, 
- interfaces

## Prérequis

Installer Node.js et npm 

## Exécuter l'application

1. **Clonez le dépôt** 

2. **Récupérez les modules**

```
npm ci
```

3. **Lancez l'application**

```bash
npx tsc && node dist/index.js
```

## Utilisation

### **Ajouter de nouveaux produits dans l'inventaire**

Définir l'ID du produit, le nom, le poids, le prix et les dimensions.

**Divers**

```ts
const lunettes = new Product(1, "lunettes", 0.2, 16, { length: 2, width: 2, height: 2 });
```

**Vêtements**

Ajouter en plus la taille du vêtement.

```ts
const tshirt = new Clothing(3, "tshirt", 0.7, 25, { length: 2, width: 2, height: 2 }, ClothingSize.size_m);
```

Tailles possibles (*ClothingSize*) : 

- size_xs = "XS",
- size_s = "S",
- size_m = "M",
- size_l = "L",
- size_xl = "XL",
- size_xxl = "XXL"


 **Chaussures**

 Ajouter en plus la pointure.

```ts
const sneakers = new Shoe(4, "sneakers", 0.8, 60, { length: 2, width: 2, height: 2 }, ShoeSize.size_40);
```

Tailles possibles (*ShoeSize*) : 

- size_36 = "36",
- size_37 = "37",
- size_38 = "38",
- size_39 = "39",
- size_40 = "40",
- size_41 = "41",
- size_42 = "42",
- size_43 = "43",
- size_44 = "44",
- size_45 = "45",
- size_46 = "46",

### **Ajouter des modes de livraisons**

Deux modes de livraisons sont disponibles : standard ou express.

**Livraison standard**

```ts
const standard = new StandardDelivery();
```

**Livraison express**

```ts
const express = new ExpressDelivery();
```

### **Créer un nouveau client** 

Créer le client (ID du client, nom, email)

```ts
const vincent = new Customer(2, "Vincent", "vincent@gmail.com");
```

Ajouter l'adresse du client (street, city, postalcode, country)

```ts
vincent.setAddress({ street: "1 rue de l'église", city: "Paris", postalCode: "75000", country: "France" });
```


### **Créer une nouvelle commande**

Indiquer l'ID de la commande, le client associée à la commande, la liste des produits commandés ([] initialement) la date de création de la commande.

```ts
const vincentOrder = new Order(1, vincent, [], "24/02/2024");
```

### **Ajouter des produits à la commande**

```ts
vincentOrder.addProduct(lunettes);
```

### **Supprimer un produit de la commande**

Préciser l'ID du produit à supprimer.
```ts
vincentOrder.removeProduct(1);
```

### **Sélectionner la livraison**

Définir la livraison pour la commande (standard ou express)

```ts
vincentOrder.setDelivery(standard);
vincentOrder.setDelivery(express);
```

