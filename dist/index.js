"use strict";
/*CUSTOMER*/
Object.defineProperty(exports, "__esModule", { value: true });
class Customer {
    customerId;
    name;
    email;
    address;
    constructor(customerId, name, email) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
    }
    /**
    *
    *@param address objet de type Address {street, city, postalCode, country}
    *@returns void (l'adresse du client est simplement renseignée)
    *
    */
    setAddress(address) {
        this.address = address;
    }
    displayAddress() {
        if (this.address) {
            return `Address: ${this.address.street} ${this.address.postalCode} ${this.address.city}, ${this.address.country}`;
        }
        else {
            return 'Address : No address found';
        }
    }
    displayInfo() {
        return `CustomerID : ${this.customerId}, Name : ${this.name}, Email : ${this.email}, ${this.displayAddress()}`;
    }
}
class Product {
    productId;
    name;
    weight;
    price;
    dimensions;
    constructor(productId, name, weight, price, dimensions) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions;
    }
    displayDetails() {
        return `productID : ${this.productId}, Name : ${this.name}, Weight : ${this.weight} kg, Price : ${this.price}€, Dimensions : ${this.dimensions.length}x${this.dimensions.width}x${this.dimensions.height}`;
    }
}
/* CLOTHING */
var ClothingSize;
(function (ClothingSize) {
    ClothingSize["size_xs"] = "XS";
    ClothingSize["size_s"] = "S";
    ClothingSize["size_m"] = "M";
    ClothingSize["size_l"] = "L";
    ClothingSize["size_xl"] = "XL";
    ClothingSize["size_xxl"] = "XXL";
})(ClothingSize || (ClothingSize = {}));
class Clothing extends Product {
    productId;
    name;
    weight;
    price;
    dimensions;
    size;
    constructor(productId, name, weight, price, dimensions, size) {
        super(productId, name, weight, price, dimensions);
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions;
        this.size = size;
    }
    displayDetails() {
        const productDetails = super.displayDetails();
        return productDetails + ` Size : ${this.size}`;
    }
}
/* SHOE */
var ShoeSize;
(function (ShoeSize) {
    ShoeSize["size_36"] = "36";
    ShoeSize["size_37"] = "37";
    ShoeSize["size_38"] = "38";
    ShoeSize["size_39"] = "39";
    ShoeSize["size_40"] = "40";
    ShoeSize["size_41"] = "41";
    ShoeSize["size_42"] = "42";
    ShoeSize["size_43"] = "43";
    ShoeSize["size_44"] = "44";
    ShoeSize["size_45"] = "45";
    ShoeSize["size_46"] = "46";
})(ShoeSize || (ShoeSize = {}));
class Shoe extends Product {
    productId;
    name;
    weight;
    price;
    dimensions;
    size;
    constructor(productId, name, weight, price, dimensions, size) {
        super(productId, name, weight, price, dimensions);
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions;
        this.size = size;
    }
    displayDetails() {
        const productDetails = super.displayDetails();
        return productDetails + ` Size : ${this.size}`;
    }
}
/* ORDER */
class Order {
    orderId;
    customer;
    productList;
    orderDate;
    delivery;
    constructor(orderId, customer, productList, orderDate) {
        this.orderId = orderId;
        this.customer = customer;
        this.productList = productList;
        this.orderDate = orderDate;
    }
    addProduct(product) {
        this.productList.push(product);
    }
    removeProduct(productId) {
        this.productList = this.productList.filter(product => product.productId !== productId);
    }
    calculateWeight() {
        let totalWeight = 0;
        this.productList.forEach(product => {
            totalWeight += product.weight;
        });
        return totalWeight;
    }
    calculateTotal() {
        let totalPrice = 0;
        this.productList.forEach(product => {
            totalPrice += product.price;
        });
        return totalPrice;
    }
    /**
    *
    *@param delivery instance standard ou express (qui implémentent l'interface Delivery)
    *@returns void
    *
    */
    setDelivery(delivery) {
        this.delivery = delivery;
    }
    displayProductList() {
        return this.productList.map(product => product.displayDetails() + "\n");
    }
    displayOrder() {
        return `orderID : ${this.orderId}
        \nOrder Date : ${this.orderDate}
        \nCustomer : \n${this.customer.displayInfo()} 
        \nProducts List : \n${this.displayProductList()} 
        \nTotal weight : ${this.calculateWeight()} kg 
        \nTotal price : ${this.calculateTotal()} €
        `;
    }
    calculateShippingCosts() {
        if (this.delivery) {
            return this.delivery?.calculateShippingFee(this.calculateWeight());
        }
    }
    estimateDeliveryTime() {
        if (this.delivery) {
            return this.delivery?.estimateDeliveryTime(this.calculateWeight());
        }
    }
}
class StandardDelivery {
    estimateDeliveryTime(weight) {
        return weight < 10 ? 7 : 10;
    }
    ;
    calculateShippingFee(weight) {
        if (weight < 1) {
            return 5;
        }
        else if (weight > 5) {
            return 20;
        }
        else {
            return 10;
        }
    }
    ;
}
class ExpressDelivery {
    estimateDeliveryTime(weight) {
        return weight <= 5 ? 1 : 3;
    }
    calculateShippingFee(weight) {
        if (weight < 1) {
            return 8;
        }
        else if (weight > 5) {
            return 30;
        }
        else {
            return 14;
        }
    }
    ;
}
/* UTILISATION */
//créé un nouveau client et lui attribue son addresse
const vincent = new Customer(2, "Vincent", "vincent@gmail.com");
vincent.setAddress({ street: "1 rue de l'église", city: "Paris", postalCode: "75000", country: "France" });
//créé une nouvelle commande
const vincentOrder = new Order(1, vincent, [], "24/02/2024");
//créé de nouveaux produits
const lunettes = new Product(1, "lunettes", 0.2, 16, { length: 2, width: 2, height: 2 });
const rasoir = new Product(2, "rasoir", 0.1, 12, { length: 2, width: 2, height: 2 });
const tshirt = new Clothing(3, "tshirt", 0.7, 25, { length: 2, width: 2, height: 2 }, ClothingSize.size_m);
const sneakers = new Shoe(4, "sneakers", 0.8, 60, { length: 2, width: 2, height: 2 }, ShoeSize.size_40);
//ajoute des produits dans la liste
vincentOrder.addProduct(lunettes);
vincentOrder.addProduct(rasoir);
vincentOrder.addProduct(tshirt);
vincentOrder.addProduct(sneakers);
console.log(vincentOrder.displayOrder());
//supprimer le produit d'id 1 (lunettes)
vincentOrder.removeProduct(1);
console.log("A product was removed from order :", vincentOrder.orderId);
console.log(vincentOrder.displayProductList());
//créé les modes de livraisons
const standard = new StandardDelivery();
const express = new ExpressDelivery();
vincentOrder.setDelivery(standard);
console.log("Standard Shipping Costs: ", vincentOrder.calculateShippingCosts());
console.log("Standard Delivery Time: ", vincentOrder.estimateDeliveryTime());
vincentOrder.setDelivery(express);
console.log("Express Shipping Costs: ", vincentOrder.calculateShippingCosts());
console.log("Express Delivery Time: ", vincentOrder.estimateDeliveryTime());
